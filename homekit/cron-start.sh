#!/bin/sh
cd /home/pi/heater-assistant/homekit/
sudo -u pi python3 heater.py >> ../logs/homekit-heater.log &
sudo -u pi python3 fan.py >> ../logs/homekit-fan.log &
exit 0