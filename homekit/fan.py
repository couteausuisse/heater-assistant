#!/usr/bin/python
# -*- coding:utf-8 -*-
"""An example of how to setup and start an Accessory.

This is:
1. Create the Accessory object you want.
2. Add it to an AccessoryDriver, which will advertise it on the local network,
    setup a server to answer client queries, etc.
"""
import logging
import signal
import random
import subprocess
from pymemcache.client.base import Client
#db = Client('127.0.0.1')
from pyhap.accessory import Accessory, Bridge
from pyhap.accessory_driver import AccessoryDriver
from pyhap.const import CATEGORY_THERMOSTAT, CATEGORY_FAN

logging.basicConfig(level=logging.INFO, format="[%(module)s] %(message)s")
class Fan(Accessory):
    """Fake garage door."""
    class AccessoryInformation:
        Manufacturer = 'couteausuisse'
        Model = 'HA1'
        SerialNumber = '898y3927621s'

    category = CATEGORY_FAN

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        db = Client('127.0.0.1')
        fan = self.add_preload_service(
         #"TemperatureDisplayUnits"
         #"HeatingThresholdTemperature",
            'Fan', chars=[
                'On',
                'Name', 
                'RotationSpeed'
                ])
        self.char_on = fan.configure_char('On', setter_callback=self.change_fan_on)
        """if db.get('miner_fan_speed') == None:
            db.set('miner_fan_speed', 0)
        if round(float(db.get('miner_fan_speed'))) == 0:
            self.char_on.set_value(False)
        else:
            self.char_on.set_value(True) """
        self.char_name = fan.configure_char('Name')
        self.char_name.set_value('Bitcoin Heater Fan Control')
        self.char_curr_speed = fan.configure_char('RotationSpeed', setter_callback=self.change_fan_speed)
        #self.char_curr_speed.set_value(float(db.get('miner_fan_avg')))

    @Accessory.run_at_interval(3)
    async def run(self):
        db = Client('127.0.0.1')
        if round(float(db.get('miner_fan_speed'))) == 0:
            self.char_on.set_value(False)
        else:
            self.char_on.set_value(True)
        self.char_curr_speed.set_value(round(float(db.get('miner_fan_avg'))))

    def change_fan_on(self, value):
        db = Client('127.0.0.1')
        logging.info("Fan value: %s", value)
        self.get_service('Fan')\
            .get_characteristic('On')\
            .set_value(value)
        if value == False:
            db.set('miner_fan_speed', 0)
            subprocess.Popen('python3 '+os.path.join(os.path.dirname(__file__))+'/../miner/set_config.py', shell = True)
    def change_fan_speed(self, value):
        db = Client('127.0.0.1')
        logging.info("Fan speed value: %s", value)
        self.get_service('Fan')\
            .get_characteristic('RotationSpeed')\
            .set_value(value)
        db.set('miner_fan_speed', value)
        subprocess.Popen('python3 '+os.path.join(os.path.dirname(__file__))+'/../miner/set_config.py', shell = True)


def get_bridge(driver):
    """Call this method to get a Bridge instead of a standalone accessory."""
    bridge = Bridge(driver, 'Bitcoin Heater Bridge')
    bridge.add_accessory(Thermostat(driver, 'Bitcoin Heater'))

    return bridge


def get_accessory(driver):
    """Call this method to get a standalone Accessory."""
    return Fan(driver, 'Bitcoin Heater Fan')

# Start the accessory on port 51826
driver = AccessoryDriver(port=51827, persist_file='main.state')

# Change `get_accessory` to `get_bridge` if you want to run a Bridge.
driver.add_accessory(accessory=get_accessory(driver))
# We want SIGTERM (terminate) to be handled by the driver itself,
# so that it can gracefully stop the accessory, server and advertising.
signal.signal(signal.SIGTERM, driver.signal_handler)

# Start it!
driver.start()