"""Starts a fake fan, lightbulb, garage door and a TemperatureSensor
"""
import logging
import signal
import random
from pymemcache.client.base import Client
db = Client('127.0.0.1')
from pyhap.accessory import Accessory, Bridge
from pyhap.accessory_driver import AccessoryDriver
from pyhap.const import (CATEGORY_FAN,
                         CATEGORY_LIGHTBULB,
                         CATEGORY_GARAGE_DOOR_OPENER,
                         CATEGORY_SENSOR,
                         CATEGORY_HEATER)


logging.basicConfig(level=logging.INFO, format="[%(module)s] %(message)s")


class TemperatureSensor(Accessory):
    """Fake Temperature sensor, measuring every 3 seconds."""

    category = CATEGORY_SENSOR

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        serv_temp = self.add_preload_service('TemperatureSensor')
        self.char_temp = serv_temp.configure_char('CurrentTemperature')

    @Accessory.run_at_interval(3)
    async def run(self):
        self.char_temp.set_value(float(db.get('temp_in')))


class FakeFan(Accessory):
    """Fake Fan, only logs whatever the client set."""

    category = CATEGORY_FAN

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Add the fan service. Also add optional characteristics to it.
        serv_fan = self.add_preload_service(
            'Fan', chars=['RotationSpeed', 'RotationDirection'])

        self.char_rotation_speed = serv_fan.configure_char(
            'RotationSpeed', setter_callback=self.set_rotation_speed)
        self.char_rotation_direction = serv_fan.configure_char(
            'RotationDirection', setter_callback=self.set_rotation_direction)

    def set_rotation_speed(self, value):
        logging.debug("Rotation speed changed: %s", value)

    def set_rotation_direction(self, value):
        logging.debug("Rotation direction changed: %s", value)

class LightBulb(Accessory):
    """Fake lightbulb, logs what the client sets."""

    category = CATEGORY_LIGHTBULB

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        serv_light = self.add_preload_service('Lightbulb')
        self.char_on = serv_light.configure_char(
            'On', setter_callback=self.set_bulb)

    def set_bulb(self, value):
        logging.info("Bulb value: %s", value)

class HeaterCooler(Accessory):
    """Fake Fan, only logs whatever the client set."""

    category = CATEGORY_HEATER

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Add the fan service. Also add optional characteristics to it.
        heater = self.add_preload_service(
            'HeaterCooler', chars=[
                'Active', 
                'CurrentHeaterCoolerState', 
                'TargetHeaterCoolerState', 
                'CurrentTemperature',
                'LockPhysicalControls',
                'Name',
                'HeatingThresholdTemperature',
                'TemperatureDisplayUnits',
                'RotationSpeed'
                ])

        self.char_active = heater.configure_char('Active')
        self.char_current_heater_cooler_state = heater.configure_char('CurrentHeaterCoolerState')
        self.char_target_heater_cooler_state = heater.configure_char('TargetHeaterCoolerState')
        self.char_current_temperature = heater.configure_char('CurrentTemperature')
        self.char_lock_physical_controls = heater.configure_char('LockPhysicalControls')
        self.char_name = heater.configure_char('Name')
        self.char_heating_threshold_temp = heater.configure_char('HeatingThresholdTemperature')
        self.char_rotation_speed = heater.configure_char('RotationSpeed')

    async def run(self):
        self.char_active.set_value(True)
        self.char_current_heater_cooler_state.set_value(1)
        self.char_lock_physical_controls.set_value(True)
        self.char_name.set_value('Bitcoin miner heater')
        self.char_heating_threshold_temp.set_value(20)

    @Accessory.run_at_interval(3)
    async def run(self):
        self.char_current_temperature.set_value(float(db.get('temp_set')))
        self.char_rotation_speed.set_value(float(db.get('miner_fan_avg')))


class GarageDoor(Accessory):
    """Fake garage door."""

    category = CATEGORY_GARAGE_DOOR_OPENER

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.add_preload_service('GarageDoorOpener')\
            .configure_char(
                'TargetDoorState', setter_callback=self.change_state)

    def change_state(self, value):
        logging.info("Bulb value: %s", value)
        self.get_service('GarageDoorOpener')\
            .get_characteristic('CurrentDoorState')\
            .set_value(value)

class HeaterCooler2(Accessory):
    """Fake garage door."""

    category = CATEGORY_HEATER

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        heater = self.add_preload_service(
            'HeaterCooler', chars=[
                'Active', 
                'CurrentHeaterCoolerState', 
                'TargetHeaterCoolerState', 
                'CurrentTemperature',
                'LockPhysicalControls',
                'Name',
                'HeatingThresholdTemperature',
                'TemperatureDisplayUnits',
                'RotationSpeed'
                ])
        self.char_active = heater.configure_char(
                'Active', setter_callback=self.change_state)
        self.char_active.set_value(1)
        self.char_curr_state = heater.configure_char('CurrentHeaterCoolerState')
        self.char_curr_state.set_value(1) ## 0 is off, 1 is idle, 2 is heating
        self.char_target_state = heater.configure_char('TargetHeaterCoolerState', setter_callback=self.change_heater_state)
        self.char_target_state.set_value(1)
        self.char_heat_thr_temp = heater.configure_char('HeatingThresholdTemperature', setter_callback=self.change_heater_temp)
        self.char_heat_thr_temp.set_value(float(db.get('temp_set')))
        self.char_temp = heater.configure_char('CurrentTemperature')


    @Accessory.run_at_interval(3)
    async def run(self):
        self.char_temp.set_value(float(db.get('temp_in')))
        if self.char_temp.get_value() < self.char_heat_thr_temp.get_value() :
            # should heat
            self.char_curr_state.set_value(2)
        else :
            self.char_curr_state.set_value(1)

    def change_state(self, value):
        logging.info("Active value: %s", value)
        self.get_service('HeaterCooler')\
            .get_characteristic('Active')\
            .set_value(value)
    def change_heater_state(self, value):
        logging.info("Heater value: %s", value)
        self.get_service('HeaterCooler')\
            .get_characteristic('TargetHeaterCoolerState')\
            .set_value(value)
    def change_heater_temp(self, value):
        logging.info("Heater temp value: %s", value)
        self.get_service('HeaterCooler')\
            .get_characteristic('HeatingThresholdTemperature')\
            .set_value(value)


def get_bridge(driver):
    bridge = Bridge(driver, 'Bridge')

    #bridge.add_accessory(LightBulb(driver, 'Lightbulb'))
    #bridge.add_accessory(FakeFan(driver, 'Big Fan'))
    #bridge.add_accessory(GarageDoor(driver, 'Garage'))
    #bridge.add_accessory(TemperatureSensor(driver, 'Sensor'))
    bridge.add_accessory(HeaterCooler2(driver, 'Heater'))

    return bridge


driver = AccessoryDriver(port=51826, persist_file='busy_home.state')
driver.add_accessory(accessory=get_bridge(driver))
signal.signal(signal.SIGTERM, driver.signal_handler)
driver.start()