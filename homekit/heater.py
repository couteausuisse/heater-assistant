#!/usr/bin/python
# -*- coding:utf-8 -*-
"""An example of how to setup and start an Accessory.

This is:
1. Create the Accessory object you want.
2. Add it to an AccessoryDriver, which will advertise it on the local network,
    setup a server to answer client queries, etc.
"""
import logging
import signal
import random
import subprocess
from pymemcache.client.base import Client
#db = Client('127.0.0.1')
from pyhap.accessory import Accessory, Bridge
from pyhap.accessory_driver import AccessoryDriver
from pyhap.const import CATEGORY_THERMOSTAT, CATEGORY_FAN

logging.basicConfig(level=logging.INFO, format="[%(module)s] %(message)s")
class Thermostat(Accessory):
    """Fake garage door."""
    class AccessoryInformation:
        Manufacturer = 'couteausuisse'
        Model = 'HA1'
        SerialNumber = '898y3927621s'

    category = CATEGORY_THERMOSTAT

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        db = Client('127.0.0.1')
        heater = self.add_preload_service(
         #"TemperatureDisplayUnits"
         #"HeatingThresholdTemperature",
            'Thermostat', chars=[
                'Name', 
                'TargetTemperature',
                'CurrentHeatingCoolingState',
                'TargetHeatingCoolingState',
                'CurrentTemperature',
                'HeatingThresholdTemperature',
                'TemperatureDisplayUnits'
                ])
        self.char_name = heater.configure_char('Name')
        self.char_name.set_value('Bitcoin Heater')
        self.char_curr_state = heater.configure_char('CurrentHeatingCoolingState')
        self.char_curr_state.set_value(0) ## 0 is off, 1 is idle, 2 is heating
        self.char_target_state = heater.configure_char('TargetHeatingCoolingState', setter_callback=self.change_heater_state)
        self.char_target_state.set_value(1)
        self.char_temp_unit = heater.configure_char('TemperatureDisplayUnits')
        self.char_temp_unit.set_value(0)
        self.char_heat_target_temp = heater.configure_char('TargetTemperature', setter_callback=self.change_heater_temp)
        self.char_heat_target_temp.set_value(float(db.get('temp_set')))
        self.char_temp = heater.configure_char('CurrentTemperature')
        self.char_temp.set_value(float(db.get('temp_in')))

    @Accessory.run_at_interval(3)
    async def run(self):
        db = Client('127.0.0.1')
        self.char_temp.set_value(float(db.get('temp_in')))
        self.char_heat_target_temp.set_value(float(db.get('temp_set')))
        if self.char_temp.get_value() < self.char_heat_target_temp.get_value() :
            # should heat
            self.char_curr_state.set_value(1)
        else :
            self.char_curr_state.set_value(0)

    def change_heater_state(self, value):
        logging.info("Heater value: %s", value)
        self.get_service('Thermostat')\
            .get_characteristic('TargetHeatingCoolingState')\
            .set_value(value)
    def change_heater_temp(self, value):
        db = Client('127.0.0.1')
        logging.info("Heater temp value: %s", value)
        self.get_service('Thermostat')\
            .get_characteristic('TargetTemperature')\
            .set_value(value)
        db.set('temp_set', value)
        subprocess.Popen('python3 '+os.path.join(os.path.dirname(__file__))+'/../miner/set_config.py', shell = True)

def get_bridge(driver):
    """Call this method to get a Bridge instead of a standalone accessory."""
    bridge = Bridge(driver, 'Bitcoin Heater Bridge')
    bridge.add_accessory(Thermostat(driver, 'Bitcoin Heater'))

    return bridge


def get_accessory(driver):
    """Call this method to get a standalone Accessory."""
    return Thermostat(driver, 'Bitcoin Heater')

# Start the accessory on port 51826
driver = AccessoryDriver(port=51826, persist_file='main.state')

# Change `get_accessory` to `get_bridge` if you want to run a Bridge.
driver.add_accessory(accessory=get_accessory(driver))
# We want SIGTERM (terminate) to be handled by the driver itself,
# so that it can gracefully stop the accessory, server and advertising.
signal.signal(signal.SIGTERM, driver.signal_handler)

# Start it!
driver.start()