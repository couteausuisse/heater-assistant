## heater-assistant
heater-assistant is a set of very simple python/bash scripts that are used to collect data and control the display for an 
at-home bitcoin mining enclosure. Made to work on any RPI. Exposes as a HeaterCooler device on HomeKit, can be integrated to Home Assistant.
![image info](info/Screenshot 2022-12-28 at 3.39.48 PM.png)
The setup script configures a baseline of assuptions for the rest of the scripts, here's a few examples:
	- make the RPI route packets between interfaces, acting as a bridge for the miner
	- initializing 2 temp sensors
	- initializing the e-ink screen

### collect
Multiple scripts are periodicaly run with cron and used to collect data from various sources:
	- ip.sh is used to get the miner's IP from the DHCP pool.	
	- miner.sh queries the miner for hashrate, temp, diff, etc.
	- sensors.sh queries a sensor for temperature & humidity.

### db
Using pickledb as a simple key-value store. No need for a traditional DB as it's only used store and display *live* data.
	
### display
I'm using a 2.7inch 250 x 176 BW e-ink display controled by SPI.
Rough layout :
``` 
+------------------------+
| POWER HASHRATE SUBSIDY |
| 920W    9.6TH    22%   |
+------------------------+     
| SET   IN   MINER   OUT |
| 22    20    34     23  | 
+------------------------+
```
The display has 4 buttons on the side, 2 will be used to set/change the temperature and the 2 others could be used for cycling between screens.

One other stat that would be interresting is the mine4heat subsidy. 
By taking into account the energy price and total energy used to heat the room we get the 'normal' heating cost. Let's say we use 800w to heat up the room, at 0.11$/kwh, equals a cost of 0.088$/h.
Next, by using the hashrate, difficulty and btc price, we can get an approximate current revenue: at 9Th, we mine 0.00000128 btc/h worth 0.02$.
By putting the two together, we get an estimate of the current mining subsidy to heat the room :
22%.
In other words, it now costs us 22% less to heat up the room compared to using a normal heater.

### thermostat
2 thermometers should be connected to the RPI, one at the ingress of the enclosure, the other a the egress.
The RPI will poll inputs for the buttons and apply an hysteresis wave on the temp, to limit the on/off jiggle for the miner.
stop and start scripts are used to send commands to the miner for quick start and stop minig operations.
The modulate script can lower or increase the power limit of the miner, thus modulating the output temperature without 
completely stopping it. Think of it as PWM instead of switching.

### webserver
A barebones static webserver can be accessed from the LAN.
It is used to set a few configs and options.
Future set of options:
	- set miner's pool and power limit
	- Make a temperature schedule
	- email alerts
	- revenue estimates
![image info](info/Screenshot 2022-12-28 at 3.39.55 PM.png)