#!/usr/bin/python
# -*- coding:utf-8 -*-
import datetime
import os
import subprocess
from pymemcache.client.base import Client
db = Client('127.0.0.1')

temp_set = float(db.get('temp_set'))
temp_variation = float(db.get('temp_variation'))
temp_in = float(db.get('temp_in'))

if (float(temp_in) > (float(temp_set) + float(temp_variation))):
    # set temp is acheived, lowering mining profile
    db.set('miner_power_step_multiplier', int(db.get('miner_power_step_multiplier'))+1) 
    print(str(datetime.datetime.now())," Too Hot, Step Mutliplier: %-3.1f C" % int(db.get('miner_power_step_multiplier')))
    subprocess.Popen('python3 '+os.path.join(os.path.dirname(__file__))+'/../miner/set_config.py', shell = True)
elif (float(temp_in) < (float(temp_set) - float(temp_variation))):
     # set temp too low, increasing mining profile
    db.set('miner_power_step_multiplier', int(db.get('miner_power_step_multiplier'))-1) 
    print(str(datetime.datetime.now())," Too Cold, Step Mutliplier: %-3.1f C" % int(db.get('miner_power_step_multiplier')))
    subprocess.Popen('python3 '+os.path.join(os.path.dirname(__file__))+'/../miner/set_config.py', shell = True)
