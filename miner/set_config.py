#!/usr/bin/python
# -*- coding:utf-8 -*-
import toml
import os
import datetime
from os import system
from pymemcache.client.base import Client
db = Client('127.0.0.1')

#get file : ssh root@10.66.10.15 "cat /etc/bosminer.toml" > bosminer.toml 
system('ssh root@'+str(db.get('miner_ip').decode('UTF-8'))+' "cat /etc/bosminer.toml" > bosminer.toml')

data = toml.load("bosminer.toml") 

fan_speed = int(db.get('miner_fan_speed'))

if fan_speed == 0:
    data['temp_control']['mode'] = 'auto'
    fan_speed = 10
else:
    #fan control is only used then temp_control is not 'auto'
    data['temp_control']['mode'] = 'manual'
data['temp_control']['target_temp'] = 45
data['temp_control']['hot_temp'] = 75
data['temp_control']['dangerous_temp'] = 95

data['fan_control']['speed'] = fan_speed

data['group'][0]['pool'][0]['url'] = str(db.get('miner_pool').decode('UTF-8'))
data['group'][0]['pool'][0]['user'] = str(db.get('miner_username').decode('UTF-8'))

if db.get('miner_power_step_multiplier') == None :
        db.set('miner_power_step_multiplier', 0)

data['autotuning']['enabled'] = True
data['autotuning']['power_target'] = (int(db.get('miner_power_limit')) - (int(db.get('miner_power_step_multiplier')) * int(db.get('miner_power_step'))))

data['performance_scaling']['enabled'] = False
data['performance_scaling']['shutdown_enabled'] = False

# To use the dump function, you need to open the file in 'write' mode
# It did not work if I just specify file location like in load
f = open("bosminer.toml",'w')
toml.dump(data, f)
f.close()
#send file: cat bosminer.toml | ssh root@10.66.10.15 "cat > /etc/bosminer.toml"
system('cat bosminer.toml | ssh root@'+str(db.get('miner_ip').decode('UTF-8'))+' "cat > /etc/bosminer.toml"')
#restart BOS
system('ssh root@'+str(db.get('miner_ip').decode('UTF-8'))+' "/etc/init.d/bosminer stop && /etc/init.d/bosminer start &"')