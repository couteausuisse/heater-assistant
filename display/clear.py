#!/usr/bin/python
# -*- coding:utf-8 -*-
import subprocess
import logging
import epaper
epd2in7 = epaper.epaper('epd2in7')
import datetime
import os
try:
    epd = epd2in7.EPD()
    logging.info("init and Clear")
    epd.init()
    epd.Clear()
    subprocess.call(os.path.join(os.path.dirname(__file__))+"/update.py", shell=True)
except IOError as e:
    logging.info(e)
    
except KeyboardInterrupt:    
    logging.info("ctrl + c:")
    epd2in7.epdconfig.module_exit()
    exit()