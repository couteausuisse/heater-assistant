#!/usr/bin/python
# -*- coding:utf-8 -*-
import pickledb
import subprocess
import shlex
import json
import sys
import os
import logging
from waveshare_epd import epd2in7
import time
from PIL import Image,ImageDraw,ImageFont
import traceback
import datetime
#logging.basicConfig(level=logging.DEBUG)
db = pickledb.load(os.path.join(os.path.dirname(__file__))+'/../heater.db', True) 

try:

    logging.info("epd2in7 Demo")   
    epd = epd2in7.EPD()
    logging.info("init and Clear")
    epd.init()
    #epd.Clear(0xFF)
    fnt = ImageFont.truetype(os.path.join((os.path.dirname(__file__)+"/SourceCodePro-Regular.ttf")), 15)
    # Drawing on the Horizontal image
    Himage = Image.new('1', (epd.height, epd.width), 255)  # 255: clear the frame
    draw = ImageDraw.Draw(Himage)
    power = str(db.get('miner_power'))+"W".center(8)
    hashrate = str(db.get('miner_hashrate_th'))+"TH".center(8)
    subsidy = str(db.get('subsidy'))+"TH".center(8)
    miner_temp = str(db.get('miner_temp_avg')).center(6)
    temp_in = str(db.get('temp_in')).center(6)
    temp_out = str(db.get('temp_out')).center(6)
    text = "+------------------------+\n| POWER HASHRATE SUBSIDY |\n|"+power+hashrate+subsidy+"|\n+------------------------+\n| SET   IN   MINER   OUT |\n| 22  "+temp_in+" "+miner_temp+""+temp_out+"|\n+------------------------+"
    draw.text((10,10), text, font=fnt, fill=0)
    epd.display(epd.getbuffer(Himage))
    time.sleep(10)

#    logging.info("Clear...")
#    epd.Clear(0xFF)
    logging.info("Goto Sleep...")
    epd.sleep()
        
except IOError as e:
    logging.info(e)
    
except KeyboardInterrupt:    
    logging.info("ctrl + c:")
    epd2in7.epdconfig.module_exit()
    exit()