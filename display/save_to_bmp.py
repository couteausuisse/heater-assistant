#!/usr/bin/python
# -*- coding:utf-8 -*-
from PIL import Image, ImageDraw, ImageFont
import os 
from pymemcache.client.base import Client
db = Client('127.0.0.1')

img = Image.new('RGB', (264, 176), color = 'white')
fnt = ImageFont.truetype(os.path.join(os.path.dirname(__file__)+"/SourceCodePro-Regular.ttf"), 15)
power = (str(int(db.get('miner_power'))).strip()+"W").center(8)
hashrate = (str(round(float(db.get('miner_hashrate_th')))).strip()+"TH").center(8)
subsidy = (str(round(float(db.get('subsidy')))).strip()+"%").center(8)
miner_temp = (str(round(float(db.get('miner_temp_avg')))).strip()).center(6)
temp_in = (str(round(float(db.get('temp_in')))).strip()).center(6)
temp_out = (str(round(float(db.get('temp_out')))).strip()).center(6)
temp_set = (str(round(float(db.get('temp_set')))).strip()).center(5)
text = "+------------------------+\n| POWER HASHRATE SUBSIDY |\n|"+power+hashrate+subsidy+"|\n+------------------------+\n| SET   IN   MINER   OUT |\n|"+temp_set+""+temp_in+" "+miner_temp+""+temp_out+"|\n+------------------------+"
print(text)
d = ImageDraw.Draw(img)
d.text((10,10), text, font=fnt, fill=0)
img.save(os.path.join(os.path.dirname(__file__))+'/../webserver/static/img/display.png')