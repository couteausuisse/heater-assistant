#!/usr/bin/python
# -*- coding:utf-8 -*-

import os
import logging
import time
import epaper
epd2in7 = epaper.epaper('epd2in7')
from PIL import Image,ImageDraw,ImageFont
import datetime
from pymemcache.client.base import Client
db = Client('127.0.0.1')

try:
    epd = epd2in7.EPD()
    epd.init()
    epd.Clear(0xFF)
    fnt = ImageFont.truetype(os.path.join(os.path.dirname(__file__)+"/SourceCodePro-Regular.ttf"), 15)
    # Drawing on the Horizontal image
    logging.info("1.Drawing on the Horizontal image...")
    Himage = Image.new('1', (epd.height, epd.width), 255)  # 255: clear the frame
    draw = ImageDraw.Draw(Himage)
    power = (str(int(db.get('miner_power'))).strip()+"W").center(8)
    hashrate = (str(round(float(db.get('miner_hashrate_th')))).strip()+"TH").center(8)
    subsidy = (str(round(float(db.get('subsidy')))).strip()+"%").center(8)
    miner_temp = (str(round(float(db.get('miner_temp_avg')))).strip()).center(6)
    temp_in = (str(round(float(db.get('temp_in')))).strip()).center(6)
    temp_out = (str(round(float(db.get('temp_out')))).strip()).center(6)
    temp_set = (str(round(float(db.get('temp_set')))).strip()).center(5)
    text = "+------------------------+\n| POWER HASHRATE SUBSIDY |\n|"+power+hashrate+subsidy+"|\n+------------------------+\n| SET   IN   MINER   OUT |\n|"+temp_set+""+temp_in+" "+miner_temp+""+temp_out+"|\n+------------------------+"
    draw.text((10,10), text, font=fnt, fill=0)
    epd.display(epd.getbuffer(Himage))
    time.sleep(2)
    logging.info("Goto Sleep...")
    epd.sleep()
        
except IOError as e:
    logging.info(e)
    
except KeyboardInterrupt:    
    logging.info("ctrl + c:")
    epd2in7.epdconfig.module_exit()
    exit()