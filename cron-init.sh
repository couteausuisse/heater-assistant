#!/bin/sh
cd /home/pi/heater-assistant/
sudo -u pi python3 init.py
sleep 30
sudo -u pi python3 collect/ip.py
sudo -u pi python3 collect/miner.py
sudo -u pi python3 collect/subsidy.py
sudo -u pi python3 collect/sensors.py
sudo -u pi python3 display/update.py
exit 0