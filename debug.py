#!/usr/bin/python
# -*- coding:utf-8 -*-
from pymemcache.client.base import Client
db = Client('127.0.0.1')

print(db.get('miner_ip').decode('UTF-8'))
print(db.get('miner_hashrate_th').decode('UTF-8'))
print(db.get('miner_fan_avg').decode('UTF-8'))
print(db.get('miner_temp_avg').decode('UTF-8'))
print(db.get('miner_power').decode('UTF-8'))
print(db.get('subsidy').decode('UTF-8'))
print(db.get('miner_pool').decode('UTF-8'))
print(db.get('miner_username').decode('UTF-8'))
print(db.get('miner_password').decode('UTF-8'))
print(db.get('miner_power_limit').decode('UTF-8'))
print(db.get('miner_power_step').decode('UTF-8'))
print(db.get('temp_set').decode('UTF-8'))
print(db.get('temp_variation').decode('UTF-8'))
print(db.get('enable_homekit').decode('UTF-8'))
print(db.get('enable_mqtt').decode('UTF-8'))
print(db.get('temp_in').decode('UTF-8'))
print(db.get('temp_out').decode('UTF-8'))
