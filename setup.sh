#!/bin/bash

# install pre-requisites
echo "----Installing pre-requisites----"
sudo apt install dnsmasq iptables bc netcat jq python3-pip python3-pil python3-numpy libavahi-compat-libdnssd-dev memcached libmemcached-tools rustc gcc musl-dev python3-dev libffi-dev cargo build-essential libssl-dev
python -m pip install --upgrade pip
sudo pip install -U pip setuptools
sudo pip3 install RPi.GPIO spidev waveshare-epaper flask toml HAP-python[QRCode] pymemcache Adafruit_DHT
sudo systemctl start memcached

## setup routing
echo "----Configuring the network interfaces----"
sudo ifconfig eth0 192.168.2.1 netmask 255.255.255.0 broadcast 192.168.2.255
sudo echo 'interface eth0
static ip_address=192.168.2.1/24
static routers=192.168.2.1
metric 300

interface wlan0
metric 200'>> /etc/dhcpcd.conf
sudo service dhcpcd restart
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
sudo touch /etc/dnsmasq.conf
sudo echo 'interface=eth0
listen-address=192.168.2.1
bind-dynamic
server=1.1.1.1
domain-needed
bogus-priv
dhcp-range=192.168.2.50,192.168.2.150,12h' >> /etc/dnsmasq.conf
sudo sed -i -e 's/#net.ipv4.ip_forward/net.ipv4.ip_forward/' /etc/sysctl.conf && sudo sysctl -p
sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
sudo iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
sudo iptables -A FORWARD -i wlan0 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT  
sudo iptables -A FORWARD -i eth0 -o wlan0 -j ACCEPT
#sudo apt-get install isc-dhcp-server
#sudo service isc-dhcp-server stop
#sudo sed -i -e 's/INTERFACESv4=""/INTERFACES="eth0"/' /etc/default/isc-dhcp-server
#sudo cp dhcp.conf /etc/dhcpcd.conf
#sudo service isc-dhcp-server start
#sudo iptables -t nat -A POSTROUTING -s 192.168.2.0/24 -o wlan0 -j MASQUERADE
sudo apt install iptables-persistent

## install the waveshare epaper hat or using cables
#e-paper cables         rpi gpio
#VCC        17
#GND        20
#DIN        19
#CLK        23
#CS         24
#DC         22
#RST        11
#BUSY       18

##activate gpio interface
echo "----Activating the gpio interface----"
sudo raspi-config
#Choose Interfacing Options -> SPI -> Yes Enable SPI interface
#sudo reboot

#demo
# echo "----Running the e-paper test----"
# git clone --depth=1 https://github.com/waveshare/e-Paper.git
# cd e-Paper/RaspberryPi_JetsonNano/python
# sudo python3 setup.py build
# sudo python3 setup.py install
# # run demo
# cd examples/
# python3 epd_2in7_test.py
# cd ~/heater-assistant/

#crontab -e @reboot /usr/bin/python3.9 /home/pi/e-Paper/RaspberryPi_JetsonNano/python/examples/epd_2in7_test.py 

##Temp sensor
#I'm installing 2 DHT11 modules, one on pin 7 and the onther on pin 10
#determination of in/out is made in software (cold/hot)

## initiate base values
python3 init.py

## cron jobs
echo "----Configuring the cron jobs----"
# generate the crontab file
echo '*/15 * * * * /usr/bin/python3 '$(pwd)'/collect/ip.py >> '$(pwd)'/logs/ip.log' >> cronjobs.txt
echo '*/2 * * * * /usr/bin/python3 '$(pwd)'/collect/miner.py >> '$(pwd)'/logs/miner.log' >> cronjobs.txt
echo '*/1 * * * * /usr/bin/python3 '$(pwd)'/collect/sensors.py >> '$(pwd)'/logs/sensors.log' >> cronjobs.txt
echo '*/1 * * * * /usr/bin/python3 '$(pwd)'/thermostat/check.py >> '$(pwd)'/logs/thermostat.log' >> cronjobs.txt
echo '*/2 * * * * /usr/bin/python3 '$(pwd)'/collect/subsidy.py >> '$(pwd)'/logs/subsidy.log' >> cronjobs.txt
echo '*/1 * * * * /usr/bin/python3 '$(pwd)'/display/update.py >> '$(pwd)'/logs/display.log' >> cronjobs.txt
echo '*/1 * * * * /usr/bin/python3 '$(pwd)'/display/save_to_bmp.py >> '$(pwd)'/logs/bmp.log' >> cronjobs.txt
echo '@reboot /usr/bin/bash rm '$(pwd)'/logs/*'
echo '@reboot /usr/bin/bash '$(pwd)'/webserver/cron-start.sh >> '$(pwd)'/logs/webserver.log' >> cronjobs.txt
echo '@reboot /usr/bin/bash '$(pwd)'/homekit/cron-start.sh >> '$(pwd)'/logs/homekit.log' >> cronjobs.txt
echo '@reboot /usr/bin/bash '$(pwd)'/cron-init.sh >> '$(pwd)'/logs/init.log' >> cronjobs.txt
# load the crontab file to crontab
crontab cronjobs.txt

echo "----Exposing the service to HomeKit/Home Assistant----"
python3 homekit/main.py &

echo "----Starting the webserver----"
python3 webserver/app.py &

sudo reboot now