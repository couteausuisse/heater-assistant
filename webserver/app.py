#!/usr/bin/python
# -*- coding:utf-8 -*-
from flask import Flask, request, render_template
import datetime
import os
import subprocess
from os import system
from pymemcache.client.base import Client
app = Flask(__name__)

@app.route('/', methods = ['GET'])
def index():
    db = Client('127.0.0.1')
    pause = 0
    if round(float(db.get('miner_power'))) == 0 :
        pause = 1
    return render_template('index.html', pause=pause)

@app.route('/dashboard', methods = ['GET'])
def dashboard():
    db = Client('127.0.0.1')
    info = {
        "miner_ip": str(db.get('miner_ip').decode('UTF-8')),
        "miner_hashrate_th": float(db.get('miner_hashrate_th')),
        "miner_fan_avg": float(db.get('miner_fan_avg')),
        "miner_temp_avg": float(db.get('miner_temp_avg')),
        "miner_power": int(db.get('miner_power')),
        "subsidy": float(db.get('subsidy'))
    }
    return render_template('dashboard.html', info=info)

@app.route('/pause', methods = ['POST'])
def pause():
    if request.method == 'POST':
        if request.form.get('pause') == 1 :
            subprocess.Popen('python3 '+os.path.join(os.path.dirname(__file__))+'/../miner/pause.py', shell = True)
        elif request.form.get('pause') == 0 :
            subprocess.Popen('python3 '+os.path.join(os.path.dirname(__file__))+'/../miner/resume.py', shell = True)
    return render_template('index.html', pause=request.form.get('pause'))


@app.route('/miner', methods = ['GET', 'POST'])
def miner():
    db = Client('127.0.0.1')
    if request.method == 'POST':
        db.set('miner_pool', request.form.get('miner_pool'))
        db.set('miner_username', request.form.get('miner_username'))
        db.set('miner_power_limit', request.form.get('miner_power_limit'))
        db.set('miner_power_step', request.form.get('miner_power_step'))
        db.set('miner_fan_speed', request.form.get('miner_fan_speed'))
        subprocess.Popen('python3 '+os.path.join(os.path.dirname(__file__))+'/../miner/set_config.py', shell = True)


    info = {
        "miner_ip": str(db.get('miner_ip').decode('UTF-8')),
        "miner_hashrate_th": float(db.get('miner_hashrate_th')),
        "miner_fan_avg": float(db.get('miner_fan_avg')),
        "miner_temp_avg": float(db.get('miner_temp_avg')),
        "miner_power": int(db.get('miner_power')),
        "subsidy": float(db.get('subsidy')),
        "miner_pool": str(db.get('miner_pool').decode('UTF-8')),
        "miner_username": str(db.get('miner_username').decode('UTF-8')),
        "miner_power_limit": int(db.get('miner_power_limit')),
        "miner_power_step": int(db.get('miner_power_step')),
        "miner_fan_speed":int(db.get('miner_fan_speed'))
    }
    return render_template('miner.html', info=info)

@app.route('/temperature', methods = ['GET', 'POST'])
def temperature():
    db = Client('127.0.0.1')
    if request.method == 'POST':
        db.set('temp_set', request.form.get('temp_set'))
        db.set('temp_variation', request.form.get('temp_variation'))
        db.set('enable_homekit', request.form.get('enable_homekit'))
        db.set('enable_mqtt', request.form.get('enable_mqtt'))

    info = {
        "temp_set": float(db.get('temp_set')),
        "temp_in": float(db.get('temp_in')),
        "miner_temp_avg": float(db.get('miner_temp_avg')),
        "temp_out": float(db.get('temp_out')),
        "temp_variation": float(db.get('temp_variation')),
        "enable_homekit": str(db.get('enable_homekit').decode('UTF-8')),
        "enable_mqtt": str(db.get('enable_mqtt').decode('UTF-8'))
    }
    return render_template('temperature.html', info=info)

@app.route('/alerts')
def alerts():
    return render_template('alerts.html')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000, debug=True)