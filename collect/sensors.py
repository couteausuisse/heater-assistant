#!/usr/bin/python
# -*- coding:utf-8 -*-
import Adafruit_DHT
import datetime
import os
sensor = Adafruit_DHT.DHT11
from pymemcache.client.base import Client
db = Client('127.0.0.1')

if(db.get('temp_out') == None):
    #temp_out is not set, setting it
    db.set('temp_out', 0.0)

if(db.get('temp_in') == None):
    #temp_in is not set, setting it
    db.set('temp_in', 0.0)

# change pin numbers if needed
""" for pin in [15]:
    humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
    if humidity is not None and temperature is not None:
        print("Temperature: %-3.1f C" % temperature)
        print("Humidity: %-3.1f %%" % humidity)

        #is the current temp higher or lower?
        if(temperature <= float(db.get('temp_in')) or float(db.get('temp_in') == 0.0)):
            #current temp is lower, setting it as new temp_in
            db.set('temp_in', temperature)
        else:
            #current temp is higher, setting it as new temp_out then
            db.set('temp_out', temperature)   
    else:
        print("Error reading sensors")
print(str(datetime.datetime.now())," Temp in: %-3.1f C" % db.get('temp_in'))
print(str(datetime.datetime.now())," Temp out: %-3.1f C" % db.get('temp_out')) """

## for single temp sensor, GPIO Pin ID
humidity, temperature = Adafruit_DHT.read_retry(sensor, 15)
if humidity is not None and temperature is not None:
    print("Temperature: %-3.1f C" % temperature)
    print("Humidity: %-3.1f %%" % humidity)
    db.set('temp_in', float(temperature))   
else:
    print("Error reading sensor in")
print(str(datetime.datetime.now())," Temp in: %-3.1f C" % float(db.get('temp_in')))

humidity, temperature = Adafruit_DHT.read_retry(sensor, 14)
if humidity is not None and temperature is not None:
    print("Temperature: %-3.1f C" % temperature)
    print("Humidity: %-3.1f %%" % humidity)
    db.set('temp_out', float(temperature))   
else:
    print("Error reading sensors")
print(str(datetime.datetime.now())," Temp out: %-3.1f C" % float(db.get('temp_out')))