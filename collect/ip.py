#!/usr/bin/python
# -*- coding:utf-8 -*-
import subprocess
import json
import os
import datetime
from pymemcache.client.base import Client
db = Client('127.0.0.1')

## query and storing the miner's data
data = json.loads(subprocess.run(os.path.join(os.path.dirname(__file__))+"/ip.sh", check=True, capture_output=True, text=True).stdout)
print(str(datetime.datetime.now())," Miner's ip is: ", data["miner_ip"].strip())
db.set('miner_ip', data["miner_ip"].strip())