#!/bin/bash
## this script gets the ip adresse leased out to the miner
IP=""
if [ $(hostname) = 'heater' ]; then
	IP=$(arp -a | grep "192.168.2" | grep -oP '(?<=\().*(?=\))')
else
	IP="10.66.10.15"
fi
echo "{\"miner_ip\":\"${IP}\"}"