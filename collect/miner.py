#!/usr/bin/python
# -*- coding:utf-8 -*-
import subprocess
import shlex
import json
import os
import datetime
from pymemcache.client.base import Client
db = Client('127.0.0.1')
## query and storing the miner's data
cmd = shlex.split(os.path.join(os.path.dirname(__file__))+'/miner.sh "'+str(db.get('miner_ip').decode('UTF-8'))+'"')
data = json.loads(subprocess.run(cmd, check=True, capture_output=True, text=True).stdout)
print(data)
print(data["miner_hashrate"])
db.set('miner_hashrate_th', float(data["miner_hashrate"]))
print(str(datetime.datetime.now()),' Miner Hashrate is:', float(data["miner_hashrate"]))
db.set('miner_fan_avg', float(data["miner_fan_avg"]))
print(str(datetime.datetime.now()),' Miner Fans avg is:', float(data["miner_fan_avg"]))
db.set('miner_temp_avg', float(data["miner_temp_avg"]))
print(str(datetime.datetime.now()),' Miner Temps avg is:', float(data["miner_temp_avg"]))
db.set('miner_power', int(data["miner_power"]))
print(str(datetime.datetime.now()),' Miner Power is:', int(data["miner_power"]))
db.set('miner_pool', str(data["miner_pool"]))
print(str(datetime.datetime.now()),' Miner Pool is:', str(data["miner_pool"]))
db.set('miner_username', str(data["miner_username"]))
print(str(datetime.datetime.now()),' Miner Username is:', str(data["miner_username"]))