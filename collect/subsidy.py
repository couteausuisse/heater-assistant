#!/usr/bin/python
# -*- coding:utf-8 -*-
import requests
import datetime
from pymemcache.client.base import Client
db = Client('127.0.0.1')

#constants
energy_price = 0.11 #in $ per KWh
price_per_wm = (energy_price*100)/60000 #energy price in W per minutes
interval = requests.get("https://blockchain.info/q/interval").json()/60
reward = (requests.get("https://blockchain.info/q/bcperblock").json()*100000000)/interval #in sats/minutes
btc_price = requests.get("https://blockchain.info/q/24hrprice").json()/100000000 #in $/sats

##load the miner info
power = int(db.get('miner_power'))
hashrate = float(db.get('miner_hashrate_th'))

# get the global hashrate (in gigahash/s)
global_hashrate = int(requests.get("https://blockchain.info/q/hashrate").json())
print(str(datetime.datetime.now()),' The global hashrate is:', global_hashrate, " GH/s")

# our miner is x% of the global hashrate
miner_hashrate_part = ((hashrate*1000)/global_hashrate)
print(str(datetime.datetime.now()),' The miner\'s part global hashrate is:', miner_hashrate_part, " %")

# there's an issuance of 6.25btc per block
# we get x% of 6.25btc for it 
miner_reward_part = miner_hashrate_part*reward #in sats/minutes
print(str(datetime.datetime.now()),' The miner\'s reward is:', miner_reward_part, " sats/minutes")

# current btc price is y$, so we earn x*y $ per block
reward_per_min = miner_reward_part*btc_price*100
print(str(datetime.datetime.now()),' The miner\'s reward is:', reward_per_min, " cents/minutes")

# it costs us power*(energy_price/60)*10 to heat the room for block duration
heating_cost = price_per_wm*power
print(str(datetime.datetime.now()),' The miner\'s heating cost is:', heating_cost, " cents/minutes")

#subsidy is the % of revenue from the heating cost
subsidy = round((reward_per_min/heating_cost)*100,0)
print(str(datetime.datetime.now()),' The miner\'s heating subsidy is:', subsidy, " %")
db.set('subsidy', float(subsidy))