#!/bin/bash
## this scripts queries the prometheus endpoint of the miner. the first argument is the ip adress
IP=$1
# for command in fans tempctrl temps tunerstatus
#     do
#              echo "${command}"
#        echo "{\"command\":\"${command}\"}" | nc $IP 4028 | jq
#     done
# https://docs.braiins.com/os/plus-en/Development/1_api.html
# [IP ADDRESS]:8081/metrics

## retreive miner hashrate info
hashrate=$(echo '{"command":"summary"}' | nc $IP 4028 | jq '.SUMMARY[0]."MHS av"')
#echo $hashrate
hashrate_th=$(echo "scale=2;${hashrate}/1000000" | bc)
#kvset miner_hashrate_th $hashrate_th
#echo "Hashrate: "$hashrate_th" TH/s"

## retreive network hashrate info
global_hashrate=$(echo '{"command":"summary"}' | nc $IP 4028 | jq '.SUMMARY[0]."Total MH"')
global_hashrate_th=$(echo "scale=2;${global_hashrate}/1000000" | bc)
miner_value=$(echo "scale=6;${hashrate}/${global_hashrate}" | bc)
#echo $global_hashrate_th" - "$miner_value

## retreive miner fans
fan1=$(echo '{"command":"fans"}' | nc $IP 4028 | jq '.FANS[0]."Speed"')
fan2=$(echo '{"command":"fans"}' | nc $IP 4028 | jq '.FANS[1]."Speed"')
fan_avg=$((($fan1+$fan2)/2))
#kvset miner_fan_avg $fan_avg
#echo "Fans: "$fan_avg"%"

## retreive miner temps
temp1=$(echo '{"command":"temps"}' | nc $IP 4028 | jq '.TEMPS[0]."Board"')
temp2=$(echo '{"command":"temps"}' | nc $IP 4028 | jq '.TEMPS[1]."Board"')
temp3=$(echo '{"command":"temps"}' | nc $IP 4028 | jq '.TEMPS[2]."Board"')
avg=3

if [ temp1 == "" ] || [ temp1 == 0 ] || [ -z "$temp1" ] || [ "$temp1" == "null" ]; then
    avg=$(( avg-1 ))
fi

if [ temp2 == "" ] || [ temp2 == 0 ] || [ -z "$temp2" ] || [ "$temp2" == "null" ]; then
    avg=$(( avg-1 ))
fi

if [ temp3 == "" ] || [ temp3 == 0 ] || [ -z "$temp3" ] || [ "$temp3" == "null" ]; then
    avg=$(( avg-1 ))
fi

if (( avg > 0 )); then
    temp_avg=$(echo "scale=2;(${temp1}+${temp2}+${temp3})/${avg}" | bc)
else 
    temp_avg=0
fi
#kvset miner_temp_avg $temp_avg
#echo "Temp: "$temp_avg" deg"

pool_url=$(echo '{"command":"pools"}' | nc $IP 4028 | jq '.POOLS[0]."URL"')
pool_user=$(echo '{"command":"pools"}' | nc $IP 4028 | jq '.POOLS[0]."User"')

## retreive miner power
power=$(echo '{"command":"tunerstatus"}' | nc $IP 4028 | jq '.TUNERSTATUS[0]."ApproximateMinerPowerConsumption"')
#kvset miner_power $power
#echo "Power: "$power"w"
echo "{\"miner_hashrate\":\"${hashrate_th}\",\"miner_fan_avg\":\"${fan_avg}\",\"miner_temp_avg\":\"${temp_avg}\",\"miner_power\":\"${power}\",\"miner_pool\":${pool_url},\"miner_username\":${pool_user}}"